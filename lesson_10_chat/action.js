const ws = new WebSocket('ws://ws-chat-itclass.herokuapp.com');
const chat = document.querySelector('.chat');
const sendButton = document.querySelector('.send');
const userName = document.querySelector('.name');
const messageText = document.querySelector('.text');

sendButton.addEventListener('click', sendMessage);
messageText.addEventListener('keyup', checkEnter);

//window.addEventListener('beforeunload', closeConnection);
window.addEventListener('beforeunload', () => ws.close());

let messages = [];

ws.onopen = () => {
    console.log('Connected');

    ws.onmessage = ({ data }) => {
        const result = JSON.parse(data);
        messages.unshift(result);
        renderMessages();
        chat.scrollTo(0, 1000000);
    }
/*
    setTimeout(() => {
        ws.send('Здрасти');
    }, 3000);
*/
}

function createTag(tagName, className, content = '') {
    const tag = document.createElement(tagName);
    tag.className = className;
    tag.innerText = content;

    return tag;
}

function getMessageTag(messageData) {
    const humanTime = moment.unix(messageData.time).format('HH:mm');
    const messageClass = messageData.name === userName.value ? 'message self' : 'message';
    const messageTag = createTag('div', messageClass);
    const messageItem = createTag('div', 'message-item');
    const name = createTag('span', 'item-name', messageData.name);
    const text = createTag('span', 'item-text', messageData.text);
    const time = createTag('span', 'time', humanTime);

    messageItem.append(name, text, time);
    messageTag.append(messageItem);

    return messageTag;
}

function renderMessages() {
    chat.innerHTML = '';
    const messageTags = messages.map(item => getMessageTag(item));
    chat.append(...messageTags);
}

function sendMessage() {
    console.log('Кнопка');
    const messageValue = messageText.value;
    const name = userName.value;

    if (messageValue && messageValue.length > 1) {
        const time = moment().unix();
        const message = {
            name,
            text: messageValue,
            time
        };
        const jsonMessage = JSON.stringify(message);
        ws.send(jsonMessage);
        console.log(jsonMessage);
        messageText.value = '';
    }
}

function checkEnter(event) {
    console.log(event);
    if (event.code === 'Enter') {
        sendMessage();
    }
}

/*
function closeConnection() {
    ws.close();
}
*/