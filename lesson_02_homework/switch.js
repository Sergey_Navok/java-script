let number;

number = prompt('Введите цифру от 1 до 5:');
const currentNumber = Number(number);
console.log(number);

if (isNaN(currentNumber) || currentNumber < 1 || currentNumber > 5) {
    console.error('Ввведите корретную цифру');
} else {
    switch (currentNumber) {
        case 1:
            console.log('Введена цифра один');
            break;
    
        case 2:
            console.log('Введена цифра два');
            break;
        
        case 3:
            console.log('Введена цифра три');
            break;
        
        case 4:
            console.log('Введена цифра четыре');
            break;
    
        case 5:
            console.log('Введена цифра пять');
            break;
    
        default:
            console.error('Что-то пошло не так...');
            break;
    }
}