const wrapper = document.querySelector('.wrapper');//Обращение к .wrapper, который в html
const elem = document.querySelector('.elem');//Обращение к .elem, который в html

elem.addEventListener('mousedown', mouseDownHandler);//Обращение по нажатию левой клавиши мыши к .elem
document.addEventListener('mouseup', mouseUpHandler);//Обращение после нажатия ко всей странице
wrapper.addEventListener('mouseleave', mouseUpHandler);//Действие после отпускания левой клавиши мыши

const wrapperDimensions = wrapper.getBoundingClientRect();

let offX = 0;//Смещение курсора по нажатию относительно .elem по оси X
let offY = 0;//Смещение курсора по нажатию относительно .elem по оси Y

/*Данные, которые будут храниться в LOCAL STORAGE. !!!ВАЖНО!!! Данные в LOCAL STORAGE храняться в виде строки
const temp = JSON.stringify(elem.style.transform);
localStorage.setItem('elem.style.transform', temp);
*/

elem.style.transform = getCordinates();//Извлечь данные из LOCAL STORAGE

function mouseDownHandler({ offsetX, offsetY}) {
    offX = offsetX;
    offY = offsetY;
    wrapper.addEventListener('mousemove', mouseMoveHandler);
    elem.classList.add('drag');
}

function mouseMoveHandler({ clientX, clientY }) {
    x = clientX - offX;
    y = clientY - offY;

    const { width, height } = wrapperDimensions;

    /*    
    elem.style.left = x > wrapperDimensions.width ? wrapperDimensions.width - 50 + 'px' : x + 'px';
    elem.style.top = y > wrapperDimensions.height ? wrapperDimensions.height - 50 + 'px' : y + 'px';
    
    elem.style.left = x > width - 50 ? width - 50 + 'px' : x + 'px';
    elem.style.top = y > height - 50 ? height - 50 + 'px' : y + 'px';
    */
    
    cordinatesLeft = x > width - 50 ? width - 50 : x;//Проверка если X выходит за пределы .wrapper
    cordinatesTop = y > height - 50 ? height - 50 : y;//Проверка если y выходит за пределы .wrapper

    elem.style.transform = `translate(${cordinatesLeft}px, ${cordinatesTop}px)`;//Передача координат
}

function mouseUpHandler() {
    wrapper.removeEventListener('mousemove', mouseMoveHandler);
    elem.classList.remove('drag');
    changeCordinates();
}

/*
Домашнее задание (исходное задание в первой версии lesson_05_drag_drop):
1) Передалать с position на transform:translate(x,y)
2) Сохранять положение элемента в GLOBAL STORAGE
3) При отпускании чтобы элемент плавно прилипал (либо падал) к одной из граней. Только не делать это попиксельно в js
*/

//ЗАДАНИЕ №2
function getCordinates() {
    const data = localStorage.getItem('elem.style.transform');//Сохранить во временную переменную data данные из LOCAL STORAGE
    if (data) {
        const parsedData = JSON.parse(data);//Проверка на пустой массив
        return parsedData;
    }
    return [];
}

function changeCordinates() {//Для изменения данных в LOCAL STORAGE нужно преобразовать значения, изменить значения и преобразовать обратно в строку
    const cordinatesString = JSON.stringify(elem.style.transform);
    localStorage.setItem('elem.style.transform', cordinatesString);
}