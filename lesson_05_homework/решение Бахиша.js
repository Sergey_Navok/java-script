const wrapper = document.querySelector('.wrapper');
const elem = document.querySelector('.elem');

const wrapperDimensions = wrapper.getBoundingClientRect();
console.log(wrapperDimensions);

elem.addEventListener('mousedown', mouseDownHandler);
document.addEventListener('mouseup', mouseUpHandler);
wrapper.addEventListener('mouseleave', mouseUpHandler);

let offX = 0;
let offY = 0;

function mouseDownHandler({offsetX, offsetY}){
  offX=offsetX;
  offY=offsetY;
  wrapper.addEventListener('mousemove', mouseMoveHandler);
  elem.classList.add('drag');
}
function mouseMoveHandler({clientX, clientY}){
  const x = clientX - offX;
  const y = clientY - offY;

  // создаем переменные для записи их последнего местанохождения
  lastX = x; 
  lastY = y;

  console.log(x);
  
  const {width, height} = wrapperDimensions;
  
  elem.style.transform = `translate(${x}px, ${y}px)`;

  if (x > width - 105) {
    elem.style.transform = `translate(${width - 105}px, ${y}px)`;
  } else if (x < 0){
    elem.style.transform = `translate(0, ${y}px)`;
  }

  if (y > height - 105) {
    elem.style.transform = `translate(${x}px, ${height - 105}px)`;
  } else if (y < 0) {
    elem.style.transform = `translate(${x}px, 0)`;
  }
  if (x < 0 && y < 0) {
    elem.style.transform = `translate(0px, 0px)`;   // левый верхний угол
  }

  if ((x > width - 105) && y < 0) {
    elem.style.transform = `translate(${width - 105}px, 0px)`;  // правый верхний угол
  }

  if (x < 0 && (y > height - 105)) {
    elem.style.transform = `translate(0px, ${height - 105}px)`; // левый нижний угол
  }

  if ((x > width - 105) && (y > height - 105)) {
    elem.style.transform = `translate(${width - 105}px, ${height - 105}px)`;  // правый нижний угол
  }
}


function mouseUpHandler(){
  wrapper.removeEventListener('mousemove', mouseMoveHandler);
  elem.classList.remove('drag');
}

window.onunload = () => {
  const place = {
    'x': lastX,
    'y': lastY,
  }
  localStorage.setItem('place', JSON.stringify(place));
}

window.onload = () => {
  if(localStorage.getItem('place') !== null){
    const place = JSON.parse(localStorage.getItem('place'));
    elem.style.transform = `translate(${place.x}px, ${place.y}px)`;
  }
}