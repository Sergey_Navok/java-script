const wrapper = document.querySelector('.wrapper');//Обращение к .wrapper, который в html
const elem = document.querySelector('.elem');//Обращение к .elem, который в html

elem.addEventListener('mousedown', mouseDownHandler);//Обращение по нажатию левой клавиши мыши к .elem
document.addEventListener('mouseup', mouseUpHandler);//Обращение после нажатия ко всей странице
wrapper.addEventListener('mouseleave', mouseUpHandler);//Действие после отпускания левой клавиши мыши

const wrapperDimensions = wrapper.getBoundingClientRect();


let offX = 0;//Смещение курсора по нажатию относительно .elem по оси X
let offY = 0;//Смещение курсора по нажатию относительно .elem по оси Y

/*Данные, которые будут храниться в LOCAL STORAGE. !!!ВАЖНО!!! Данные в LOCAL STORAGE храяться в виде строки
const leftTemp = JSON.stringify(elem.style.left);
localStorage.setItem('elem.style.left', leftTemp);
const topTemp = JSON.stringify(elem.style.top);
localStorage.setItem('elem.style.top', topTemp);
*/

elem.style.left = getLeftCordinates();//Извлечь данные из LOCAL STORAGE
elem.style.top = getTopCordinates();

/*
Очистить LOCAL STORAGE
storage.clear();
*/

function mouseDownHandler({ offsetX, offsetY}) {
    offX = offsetX;
    offY = offsetY;
    wrapper.addEventListener('mousemove', mouseMoveHandler);
    elem.classList.add('drag');
}

function mouseMoveHandler({ clientX, clientY }) {
    x = clientX - offX;
    y = clientY - offY;

    const { width, height } = wrapperDimensions;

    /*    
    elem.style.left = x > wrapperDimensions.width ? wrapperDimensions.width - 50 + 'px' : x + 'px';
    elem.style.top = y > wrapperDimensions.height ? wrapperDimensions.height - 50 + 'px' : y + 'px';
    */
    elem.style.left = x > width - 50 ? width - 50 + 'px' : x + 'px';
    elem.style.top = y > height - 50 ? height - 50 + 'px' : y + 'px';
}

function mouseUpHandler() {
    wrapper.removeEventListener('mousemove', mouseMoveHandler);
    elem.classList.remove('drag');
    changeCoordinates();
    //setTimeout(moveTop, 3000);//Установить ожидание 3сек до выполнения функции moveTop
    //moveTop();
}

/*
Домашнее задание:
1) Передалать с position на transform:translate(x,y)
2) Сохранять положение элемента в GLOBAL STORAGE
3) При отпускании чтобы элемент плавно прилипал (либо падал) к одной из граней. Только не делать это попиксельно в js
*/


//ЗАДАНИЕ №2
function getLeftCordinates() {
    const data = localStorage.getItem('elem.style.left');//Сохранить во временную переменную data данные из LOCAL STORAGE
    if (data) {
        const parsedData = JSON.parse(data);//Проверка на пустой массив
        return parsedData;
    }
    return [];
}

function getTopCordinates() {
    const data = localStorage.getItem('elem.style.top');//Сохранить во временную переменную data данные из LOCAL STORAGE
    if (data) {
        const parsedData = JSON.parse(data);//Проверка на пустой массив
        return parsedData;
    }
    return [];
}

function changeCoordinates() {//Для изменения данных в LOCAL STORAGE нужно преобразовать значения, изменить значения и преобразовать обратно в строку
    const cordinatesLeftString = JSON.stringify(elem.style.left);
    localStorage.setItem('elem.style.left', cordinatesLeftString);
    const cordinatesTopString = JSON.stringify(elem.style.top);
    localStorage.setItem('elem.style.top', cordinatesTopString);
}
//ЗАДАНИЕ №2



//ЗАДАНИЕ №3
function moveTop(elem) {
    
    //elem.style.transition = top 1s ease-out 0.5s;
    /*
    while (elem.style.top = 0) {
        elem.style.top--;
        console.log(elem.style.top);
    }
    */
}
//ЗАДАНИЕ №3
