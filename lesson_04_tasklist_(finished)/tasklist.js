let tasks = [
    /*
    СКОПИРОВАЛИ В ЛОКАЛ СТОРЕДЖ
    {
        id: 55,
        title: 'Купить пива',
        description: '5л светлого',
        date: new Date().getTime(),
        isComplete: false
    },
    {
        id: 66,
        title: 'Уборка в квартире',
        description: 'со шваброй',
        date: new Date().getTime(),
        isComplete: false
    },
    {
        id: 77,
        title: 'Принять шахбалай',
        description: 'От старшего брата',
        date: new Date().getTime(),
        isComplete: true
    },
    {
        id: 88,
        title: 'Сделать хорошо',
        description: 'С девушкой',
        date: new Date().getTime(),
        isComplete: false
    },
    {
        id: 99,
        title: 'Сделать плохо',
        description: 'С кем нибудь',
        date: new Date().getTime(),
        isComplete: true
    },
    */
];

const containerTag = document.querySelector('.container');
const addButton = document.querySelector('#add');
//ПЕРЕНЕС 2 СТРОКИ
const titleInput = document.querySelector('#title');
const descriptionInput = document.querySelector('#description');

//addButton.addEventListener('click', addTask);//УБРАЛ
addButton.addEventListener('click', checkTitle);//НОВОЕ
titleInput.addEventListener('keypress', titleEnterHandle);//НОВОЕ
descriptionInput.addEventListener('keypress', descriptionEnterHandle);

//НОВЫЕ 2 СТРОКИ
/*
const temp = JSON.stringify(tasks);
localStorage.setItem('tasks', temp); преобразование в локальный файл
*/
tasks = getTask();//ДОСТАТЬ ИЗ ЛОКАЛ СТРОРАДЖ


renderTasks(tasks);
changeTasks();

// function getTaskCard(taskObj) {
//     const frendlyDate = moment (taskObj.date).format('DD/MM/YYYY HH:mm');
//     const checked = taskObj.isComplete ? 'checked' : '';
//     const taskCardHtml = 
//     `<div>
//         <div class="task">
//             <input type="checkbox" ${checked}>

//             <div class="info">
//                 <span class="title">${taskObj.title}</span>
//                 <span class="subs">${taskObj.description}</span>
//             </div>
//             <span class="datetime">${frendlyDate}</span>
//         </div>
//     </div>`
//     return taskCardHtml;
// }

function createTag(tagName, className, value) {
    const tag = document.createElement(tagName);
    tag.classList.add(className);
    if (value) {
        tag.innerText = value;
    }

    return tag;
}



function getTaskCard(taskObj) {

    const taskHtml = createTag('div', 'task');

    const inputHtml = document.createElement('input');
    inputHtml.type = 'checkbox';
    inputHtml.checked = taskObj.isComplete;
    inputHtml.addEventListener('change', changeTaskState);//НОВОЕ

    const infoHtml = createTag('div', 'info');

    const titleHtml = createTag('span', 'title', taskObj.title);

    const subsHtml = createTag('span', 'subs', taskObj.description);

    const frendlyDate = moment(taskObj.date).format('DD/MM/YYYY HH:mm');
    const timeHtml = createTag('span', 'datetime', frendlyDate);

    const deleteBtn = createTag('div', 'delete', 'x');
    deleteBtn.id = taskObj.id;
    deleteBtn.addEventListener('click', deleteItem);

    infoHtml.appendChild(titleHtml);
    infoHtml.appendChild(subsHtml);
    taskHtml.appendChild(inputHtml);
    taskHtml.appendChild(infoHtml);
    taskHtml.appendChild(timeHtml);
    taskHtml.appendChild(deleteBtn);


    return taskHtml;
}


function renderTasks(tasksArr) {
    containerTag.innerHTML = '';
    tasksArr.forEach(item => {
        const taskCardHtml = getTaskCard(item);
        containerTag.appendChild(taskCardHtml);
    });
}

function deleteItem(event) {
    //tasks.splice(index, 1);
    const id = +event.target.id;
    // const currentTask = tasks.find(item => item.id === id);
    // const index = tasks.indexOf(currentTask);
    // tasks.splice(index, 1);
    tasks = tasks.filter(item => item.id !== id);

    renderTasks(tasks);
    changeTasks();
}


function addTask() {
    /*
    УБРАЛ ВЫШЕ
    const titleInput = document.querySelector('#title');
    const descriptionInput = document.querySelector('#description');
    */

    const title = titleInput.value;
    const description = descriptionInput.value;

    const newTask = getNewTask(title, description);//НОВОЕ

    /*
    УБРАЛ
    const newTask = {
        id: getNewId(),//ЗАМЕНИЛ
        title,
        description,
        date: new Date().getTime(),
        isComplete: false
    };
    */

    tasks.push(newTask);//НОВОЕ
    //tasks = [...tasks, newTask] создает новый массив и добавляет в конец новый элемент. Нужно для фреймворков иначе ничего не поменяется. 
    //tasks = [newTask, ...tasks] создает новый массив и добавляет в начало новый элемент.

    clearFields(titleInput, descriptionInput);//НОВОЕ

    //console.log(newTask); УБРАЛ
    renderTasks(tasks);
    changeTasks();
}

//НОВОЕ

function getNewId() {
    //let max = 0;
    //tasks.forEach(item => {
    //    if (item.id > max) {
    //        max = item.id;
    //    }
    //});

    //return max + 1;

    if (tasks.length > 0) {
        const ids = tasks.map(item => item.id); // [5, 8, 12] 
        const max = Math.max(...ids);
        return max + 1;
    }
    return 0;
}

function clearFields(titleField, descrField) {
    titleField.value = '';
    descrField.value = '';
}

function getNewTask(title, description) {
    const newTask = {
        id: getNewId(),
        title,
        description,
        date: new Date().getTime(),
        isComplete: false
    };

    return newTask;
}

function titleEnterHandle(event) {//добавление ввода по нажатию enter
    if (event.keyCode === 13) {
        descriptionInput.focus();//перескакивание фокуса на следующую строку
    }
}

function descriptionEnterHandle(event) {
    if (event.keyCode === 13) {
        checkTitle();
        titleInput.focus();
    }
}

function checkTitle() {
    if (titleInput.value.length > 0) {
        addTask();
        titleInput.classList.remove('required');
    } else {
        titleInput.classList.add('required');
    }
}

function getTask() {
    const data = localStorage.getItem('tasks');//ДОСТАТЬ ДАННЫЕ ИЗ ЛОКАЛ СТОРАДЖ
    if (data) {
        const parsedData = JSON.parse(data);//ПРОВРЕКА НА ПУСТОЙ МАССИВ
        return parsedData;
    }
    return [];
}

function changeTasks() {//ДЛЯ ИЗМЕНЕНИЯ ЛОКАЛЬНОГО МАССИВА НАДО ПРЕДОБРАЗОВАТЬ, ИЗМЕНИТЬ, И ПРЕОБРАЗОВАТЬ ОБРАТНО
    const tasksString = JSON.stringify(tasks);
    localStorage.setItem('tasks', tasksString);
}

function changeTaskState(event) {
    const id = +event.target.offsetParent.children[3].id;
    const task = tasks.find(item => item.id === id);
    task.isComplete = event.target.checked;
    changeTasks();
}

