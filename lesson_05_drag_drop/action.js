const wrapper = document.querySelector('.wrapper');//обращение к wrapper, который в html
const elem = document.querySelector('.elem');//обращение к elem, который в html

elem.addEventListener('mousedown', mouseDownHandler);//обращение по нажатию к elem
document.addEventListener('mouseup', mouseUpHandler);//обращение после нажатия ко всей странице
wrapper.addEventListener('mouseleave', mouseUpHandler);//отпуск левой клавиши мыши

const wrapperDimensions = wrapper.getBoundingClientRect();

let offX = 0;//смещение курсора по нажатию относительно elem по оси X
let offY = 0;//смещение курсора по нажатию относительно elem по оси Y

function mouseDownHandler({ offsetX, offsetY}) {
    offX = offsetX;
    offY = offsetY;
    wrapper.addEventListener('mousemove', mouseMoveHandler);
    elem.classList.add('drag');
}

function mouseMoveHandler({ clientX, clientY }) {
    const x = clientX - offX;
    const y = clientY - offY;

    const { width, height } = wrapperDimensions;

    /*    
    elem.style.left = x > wrapperDimensions.width ? wrapperDimensions.width - 50 + 'px' : x + 'px';
    elem.style.top = y > wrapperDimensions.height ? wrapperDimensions.height - 50 + 'px' : y + 'px';
    */
    elem.style.left = x > width - 50 ? width - 50 + 'px' : x + 'px';
    elem.style.top = y > height - 50 ? height - 50 + 'px' : y + 'px';
}

function mouseUpHandler() {
    wrapper.removeEventListener('mousemove', mouseMoveHandler);
    elem.classList.remove('drag');
}

/*
1й) ДЗ передалть с position на transform:translate(x,y)
2й) сохранять положение элемента в global storage
3й) при отпускании чтобы элемент плавно прилипал (либо падал) к одной из граней. Только не делать это попиксельно в js
*/