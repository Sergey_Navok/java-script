/*
const prom = new Promise( (resolve, reject) => { //Promise - асинхронный и срабатывает после выполнения всего
    //resolve(10); //В какой вариант попадает, тот срабатывает, до второго варианта мы не доберемся
    //reject('Alert');

   setTimeout(() => {
       resolve(10);
   }, 1000);

   reject('Alert');
});

prom
    .then(data => console.log('THEN: ', data)) //метод then, data переменная, в которую попадет результат
    .catch(err => console.log('ERR: ', err))
    .finally(() => console.log('FINAL'));
*/


/*
function getBackendData (url) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest(); //xhr - результат работы API браузера
        xhr.open('GET', url);

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    const result = xhr.responseText;
                    resolve(result);
                } else {
                    reject('ERROR: ', xhr.status);
                }
            } 
        }
        xhr.send();
    });
}
*/

const channelsBlock = document.querySelector('.channels');
const genresBlock = document.querySelector('.genres');
const tvshowsBlock = document.querySelector('.tvshows');

genresBlock.addEventListener('change', filterChannels);
channelsBlock.addEventListener('click', loadTvshows);

let allChannels = [];
let tvshows = [];
let currentTvshowIndex = 0;

console.log('Start loading...');

Promise.all([channels$, genres$]).then(([data, genres]) => {
    allChannels = data.channels;
    renderItems(channelsBlock, allChannels, getChannelCard);
    //console.log(data);
    const allGenres = addAllGenres(genres);
    renderGenres(allGenres);
    //renderGenres(genres);
    //console.log(genres);
    startInterval();
})
.catch(err => console.log(err))

function addAllGenres(genres) {
    //genres.unshift({id: 1, name: 'All genres'}); //Портит исходный массив
    const allGenres = {id: 1, name: 'All genres'};
    const newGenres = [allGenres, ...genres];
    return newGenres;
}

function getChannelCard({ logo, name, channel_id }) { //деструктурируем channel на лету и берем из него сразу logo и name
    const channelCard = getTag('div', 'channel');
    channelCard.id = channel_id;
    const logoTag = getTag('img', 'logo', logo);
    const nameTag = getTag('span', 'ch-name', name);
    channelCard.append(logoTag, nameTag);

    return channelCard;
}

/*
function renderChannels(channels) {
    channelsBlock.innerHTML = '';
    /*
    channels.forEach(item => {
        const channelCard = getChannelCard(item);
        channelsBlock.appendChild(channelCard);
    });
    */
/*    
   const channelCards = channels.map(item => getChannelCard(item));
   channelsBlock.append(...channelCards);
}
*/

function renderGenres(genres) {
    genresBlock.innerHTML = '';
    const genreTags = genres.map(item => {
        const option = getTag('option', 'option', item.name);
        option.value = item.id;

        return option;
    });

    genresBlock.append(...genreTags);
}

function filterChannels(event) {
    const genreId = Number(event.target.value);
    if (genreId === 1) {
        renderItems(channelsBlock, allChannels, getChannelCard);
    } else {
        const filteredChannels = allChannels.filter(item => item.genres.includes(genreId));
        renderItems(channelsBlock, filteredChannels, getChannelCard);
    }
}

function loadTvshows(event) {
    //console.log(event);
    const channelId = event.target.className.includes('channel') ? event.target.id : event.target.parentNode.id;
    const currentDate = moment().format('YYYY-MM-DD');    
    //console.log(currentDate);
    getTvshows(channelId, currentDate, currentDate)
        .then(data => {
            tvshows = data.tvshows.items;
            renderItems(tvshowsBlock, tvshows, createShowCard);
            renderCurrent();
            scrollToCurrent();
        });
}

//loadTvshows(30);

function createShowCard(tvshow) {
    const startTime = moment.unix(tvshow.start).format('HH:mm');
    const endTime = moment.unix(tvshow.stop).format('HH:mm');
    //const currentTime = moment().unix();
    //const isCurrent = currentTime >= tvshow.start && currentTime < tvshow.stop;

    //console.log(isCurrent);
    
    //const cardClass = isCurrent ? 'show current-show' : 'show';
    const card = getTag('div', 'show');
    const name = getTag('span', 'show-name', tvshow.title);
    const info = getTag('div', 'show-info');
    const start = getTag('span', 'time', startTime);
    const stop = getTag('span', 'time', endTime);
    const progress = getTag('div', 'progress');
    const progressLine = getTag('div', 'progress-line');

    /*
    if (isCurrent) {
        console.log(getProgress(tvshow));
        progressLine.style.width = getProgress(tvshow) + '%';
    }
    */

    progress.append(progressLine);

    info.append(start, progress, stop);
    card.append(name, info);

    return card;
}

/*
function renderTvshows(tvshows) {
    tvshowsBlock.innerHTML = '';
    const showTags = tvshows.map(item => createShowCard(item));
    tvshowsBlock.append(...showTags);
}
*/

function renderItems(renderBlock, items, creator) {
    renderBlock.innerHTML = '';
    const tags = items.map(item => creator(item));
    renderBlock.append(...tags);
}

function getProgress({ start, stop }) {
    const currentTime = moment().unix();//получаем текущее время в unix
    const progress = Math.round((currentTime - start) / (stop - start) * 100);
    
    return progress;
}

function startInterval() {
    const interval = setInterval(renderCurrent, 1000);

    return interval;
}

function renderCurrent() {
    const currentTime = moment().unix();
    const tvshowIndex = tvshows.findIndex(tvshow => currentTime >= tvshow.start && currentTime < tvshow.stop);
    const allTvshows = document.querySelectorAll('.show');

    if (currentTvshowIndex != tvshowIndex) {
        const prevShow = allTvshows[currentTvshowIndex];
        if (prevShow) {
            prevShow.classList.remove('current-show');
            currentTvshowIndex = tvshowIndex;
        }
    } else {
        currentTvshowIndex = tvshowIndex;
    }

    if (allTvshows[tvshowIndex]) {
        allTvshows[tvshowIndex].classList.add('current-show');
        document.querySelector('.current-show .progress-line').style.width = getProgress(tvshows[tvshowIndex]) + '%'; 
    }
}

function scrollToCurrent() {
    const currentShow = document.querySelector('.current-show');
    if (currentShow) {
        currentShow.scrollIntoView({ block: 'center', behavior: 'smooth' });//Найти ошибку
    }
}