function getTag(tagName, className, content = '') {
    const tag = document.createElement(tagName);
    tag.className = className;

    switch (tagName) {
        case 'img':
            tag.src = content;
            break;

        case 'input':
            tag.value = content;
            break;
        
        default:
            tag.innerText = content;
            break;
    }

    return tag;
}


//Рекурсия
const items = {
    value: 5,
    item: {
        value: 2,
        item: {
            value: 1,
            item: {
                value: 10,
                item: {
                    value: 2,
                    item: null
                }
            }
        }
    }
};

function getSumm(obj) {
    if (obj.item) {
        //debugger;
        return obj.value + getSumm(obj.item);
    }
    return obj.value;
}

const result = getSumm(items);
console.log(result);