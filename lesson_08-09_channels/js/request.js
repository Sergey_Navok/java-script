const BASE_URL = 'https://api.persik.by/v2/';

const channels$ = fetch(`${BASE_URL}content/channels`, { method: 'GET'})
    .then(response => response.json());
    /*
    .then(data => renderChannels(data.channels)) //then можно вызывать много раз, если он передает одни и теже данные
    .catch(err => console.log(err))
    .finally(() => console.log('Stop loading!'));
    */


const genres$ = fetch(`${BASE_URL}categories/channel`, { method: 'GET' })
    .then(response => response.json());
    /*
    .then(data => console.log(data))
    .catch(err => console.log(err))
    .finally(() => console.log('Stop loading!'));
    */

function getTvshows(channelId, from, to) {
    return fetch(`${BASE_URL}epg/tvshows?limit=1000&channels[]=${channelId}&from=${from}&to=${to}`, { method: 'GET'})
            .then(response => response.json());
}