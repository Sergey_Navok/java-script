function getTag(tagName, className, content = '') {
    const tag = document.createElement(tagName);
    tag.className = className;

    switch (tagName) {
        case 'img':
            tag.src = content;
            break;

        case 'input':
            tag.value = content;
            break;

        default:
            tag.innerText = content;
            break;
    }

    return tag;
}