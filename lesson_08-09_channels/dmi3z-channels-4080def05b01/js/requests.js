const channels$ = fetch('https://api.persik.by/v2/content/channels', { method: 'GET' })
    .then(response => response.json());

const genres$ = fetch('https://api.persik.by/v2/categories/channel', { method: 'GET' })
    .then(response => response.json());