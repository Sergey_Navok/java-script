const channelsBlock = document.querySelector('.channels');
const genresBlock = document.querySelector('.genres');

genresBlock.addEventListener('change', filterChannels);

let allChannels = [];

Promise.all([channels$, genres$])
    .then(([data, genres]) => {
        allChannels = data.channels;
        renderChannels(allChannels);
        renderGenres(genres);
    })
    .catch(err => console.log(err));


function getChannelCard({ logo, name }) {
    const channelCard = getTag('div', 'channel');
    const logoTag = getTag('img', 'logo', logo);
    const nameTag = getTag('span', 'ch-name', name);
    channelCard.append(logoTag, nameTag);

    return channelCard;
}

function renderChannels(channels) {
    channelsBlock.innerHTML = '';
    const channelCards = channels.map(item => getChannelCard(item));
    channelsBlock.append(...channelCards);
}

function renderGenres(genres) {
    genresBlock.innerHTML = '';
    const genreTags = genres.map(item => {
        const option = getTag('option', 'option', item.name);
        option.value = item.id;
        return option;
    });

    genresBlock.append(...genreTags);
}

function filterChannels(event) {
    const genreId = Number(event.target.value);
    const filteredChannels = allChannels.filter(item => item.genres.includes(genreId));
    renderChannels(filteredChannels);
}