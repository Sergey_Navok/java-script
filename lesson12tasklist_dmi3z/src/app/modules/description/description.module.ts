import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DescriptionComponent } from './description.component';
import { NgModule } from "@angular/core";

@NgModule({
  declarations: [
    DescriptionComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: DescriptionComponent
      }
    ])
  ]
})

export class DescriptionModule { }
