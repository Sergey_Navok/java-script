import { Task } from 'src/app/interfaces/task.interface';
import { DataService } from 'src/app/services/data.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit, OnDestroy {

  //private activetedSubscriber: Subscription;

  public taskInfo: Observable<Task>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService)
   { }

  ngOnInit(): void {
    //this.activatedRoute.params
    //    .pipe(take(1))//взять одно значение
    //    .subscribe(result => console.log(result));

    const id = this.activatedRoute.snapshot.params.id;
    //console.log(id);
    this.dataService.getTaskInfo(+id).subscribe(console.log);


  }

  ngOnDestroy(): void {
    console.log('Destroy');

    //this.activetedSubscriber.unsubscribe();
  }

}
