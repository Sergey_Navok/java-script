import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/interfaces/task.interface';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  public taskList: Task[] = [];

  constructor(private dataService: DataService) {}

  public ngOnInit(): void {
    this.dataService.getTasks().subscribe(data => this.taskList = data);
  }

  public deleteItem(id: number): void {
    this.taskList = this.taskList.filter(item => item.id !== id);
  }

  public addNewTask(task: Task): void {
    // this.taskList.push(task);
    this.taskList = [...this.taskList, task];
  }

}
