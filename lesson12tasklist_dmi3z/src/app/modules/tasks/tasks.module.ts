import { RouterModule } from '@angular/router';
import { EnterPressDirective } from './../../directives/enter.directive';
import { FirstDirective } from './../../directives/first.directive';
import { AddTaskComponent } from './../../components/add-task/add-task.component';
import { TaskComponent } from './../../components/task/task.component';
import { NgModule } from '@angular/core';
import { DateTimePipe } from 'src/app/pipes/datetime.pipe';
import { TasksComponent } from './tasks.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    TaskComponent,
    DateTimePipe,
    AddTaskComponent,
    FirstDirective,
    EnterPressDirective,
    TasksComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: TasksComponent
      }
    ])
  ]
})

export class TasksModule { }
