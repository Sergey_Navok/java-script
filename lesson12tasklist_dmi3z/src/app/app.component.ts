import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private router: Router) {} //Подключаем сервис роутингов

  /*
  public goToTasks(): void {
    this.router.navigate(['tasks']); //проверяют в app-routing.module.ts
  }

  public goToAbout(): void {
    this.router.navigate(['about']);
  }
  */

  /*
  public goTo(adress: string): void {
    this.router.navigate([adress]);
  }
  */
}
