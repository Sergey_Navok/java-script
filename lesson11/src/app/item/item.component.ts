import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: 'app-item',
  templateUrl: 'item.component.html',
  styleUrls: ['item.component.scss']
})


export class itemComponent {
  @Input() public value: number;
  @Output() public selectEvent = new EventEmitter<number>(); //@Output() public selectEvent: EventEmitter<number> = new EventEmitter(); //для передачи обратно от дочернего к родителю

  public selectValue(): void {
    this.selectEvent.next(this.value);
  }
}
