//События
const listBlock = document.querySelector('.list');
const modal = document.querySelector('.modal');
const closeModalBtn = document.querySelector('.close');
const modalWindow = document.querySelector('.modal-window');

const addFields = document.querySelectorAll('.add-field'); //забираем все элементы с одинаковым классом
const addButton = document.querySelector('.add-btn');
const openModalBtn = document.querySelector('.add-user');

const genderDropdown = document.querySelector('.gender');
const ascBtn = document.querySelector('.asc');
const descBtn = document.querySelector('.desc');

//Запрос на сервер
const xhr = new XMLHttpRequest(); //Инструмент (api браузера), через который делается запрос на бэкэнд. Класс new

//Массив обьектов
let users = [];

//Слушатели
listBlock.classList.add('loading');
listBlock.addEventListener('click', onListClick); //вешаем на клике вызов функции
closeModalBtn.addEventListener('click', closeModal); //закрытие на крестик
modal.addEventListener('click', closeModal); //закрытие на клик
modalWindow.addEventListener('click', stopProp); //остановка всплытия событий
addButton.addEventListener('click', addUser);
openModalBtn.addEventListener('click', () => modal.classList.add('show'));
//openModalBtn.addEventListener('click', () => modal.style.display = 'flex'); //style - набор стилей (не обязательно styles.css) //передаем класс напрямую в HTML значение display: flex
genderDropdown.addEventListener('change', changeGender);
ascBtn.addEventListener('click', sortAsc);
descBtn.addEventListener('click', sortDesc);

xhr.open('GET', 'https://randomuser.me/api?results=100'); //используются методы GET, POST, PUT, DELETE, PATCH
//Статусы запросов 200 - ответ получен, 400 - не найдено, 500 - запрос верный, но беда на сервере
//?results=10&gender=male - запрос на 10 пользователей-мужчин с бэка

xhr.onreadystatechange = function () { //функция показывает когда отрабатывает сервер и срабатывает при каждом новом запросе
    //xhr.addEventListener('readystatechange', func); //callback функция на каждое изменение статуса, на которую повесили слушателя, но по факту это одна и та же функция. что и выше
    //console.log('State change...', xhr.readyState); //вывести на консоль статусы изменения/готовности (0-4) - 4 запрос успешен, 3 - идет ответ от сервера, 2 - запрос отправлен
    //console.log('State change...', xhr.status); //вывести на консоль статус

    if (xhr.readyState === 4) { //проверка. что запрос выполнен
        if (xhr.status === 200) { //проверка, что данные получены
            const response = xhr.responseText; //сохраняем данные от сервера в виде строки
            const data = JSON.parse(response); //сохраняем данные от сервера как массив
            users = data.results;
            renderUsers(data.results);
            renderGenderOptions(data.results);
            listBlock.classList.remove('loading');
        }
    }
}

xhr.send(); //запуск запроса на сервер

function getUserCard(user) { //если в функции есть get, то должен быть return
    //const card = document.createElement('div');
    //card.classList.add('user');
    const card = getTag('div', 'user');
    const info = getTag('div', 'info');
    const fullName = user.name.first + ' ' + user.name.last;
    const name = getTag('span', 'name', fullName);
    const email = getTag('span', 'email', user.email);
    const age = getTag('span', 'age', user.dob.age);
    const avatar = getTag('img', 'avatar');
    const deleteBtn = getTag('div', 'delete', 'X');
    const editBtn = getTag('div', 'edit');

    const nameField = getTag('input', 'field');
    nameField.value = fullName;
    const emailField = getTag('input', 'field');
    emailField.value = user.email;
    const ageField = getTag('input', 'field');
    ageField.value = user.dob.age;

    avatar.src = user.picture.medium;
    card.id = user.login.uuid; //в div с классом user добавляем уникальный ключ uuid

    //info.append(name, email, age); //добавли в div.info
    info.append(name, email, age, nameField, emailField, ageField); //добавли в div.info

    card.append(avatar, info, deleteBtn, editBtn);

    return card;
}

function getTag(tagName, className, content = null) { //null - значение по умолчанию, и оно становится необязательным
    const newTag = document.createElement(tagName);
    newTag.classList.add(className);
    newTag.innerText = content;

    return newTag;
}

function renderUsers(users) {
    listBlock.innerHTML = ''; //чистим ListBlock
    // users.forEach(item => {
    //     const userCard = getUserCard(item);
    //     listBlock.append(userCard);
    // });

    const userCards = users.map(item => getUserCard(item)); // метод map - на основании исходного массива - формирует новый массив, либо возвращает пустой массив если ничего не сделали
    //item => getUserCard(item) - передаем на каждой итерации передает данные в userCard
    listBlock.append(...userCards); //разбираем массив на элементы
}

function onListClick(event) { //проверяем, где произошел клик
    //console.log(event);
    if (event.target.className === 'delete') { //проверка на то, что клик произошел клик на div.delete
        //console.log(event.target.parentNode.id);//ищем уникальный id для удаления
        const id = event.target.parentNode.id; //забираем id для удаления
        deleteUser(id);
    }

    if (event.target.className === 'edit') { //проверяем, клик на кнопке edit
        const infoBlock = event.target.parentNode.childNodes[1];
        const className = infoBlock.className;
        console.log(event);
        const isEditable = className.includes('editable');
        if (isEditable) {
            infoBlock.classList.remove('editable');
            const inputs = [...infoBlock.childNodes].slice(3); //отрезаем от массива необходимый кусок (от какого элемента ***и сколько элементов***) не изменяя исходный массив
            console.log(inputs);
            saveChanges(inputs, event.target.parentNode.id);
        } else {
            infoBlock.classList.add('editable');
        }
    }
}

function deleteUser(userId) {
    users = users.filter(item => item.login.uuid !== userId); //удаление быстрым способом через метод filter 
    renderUsers(users);
}

function saveChanges([name, email, age], id) { //разбираем массив inputs сразу на элементы и присваем этим элементам нужные классы
    console.log(email.value);
    console.log(id);
    const user = users.find(item => item.login.uuid === id);
    user.email = email.value;
    user.dob.age = age.value;

    const nameArr = name.value.split(' '); //разделить ввод по символу пробел
    user.name.first = nameArr[0] ? nameArr[0] : 'Empty';
    user.name.last = nameArr[1] ? nameArr[1] : '';

    renderUsers(users);
}

function closeModal() {
    modal.classList.remove('show'); //Удаляем класс show
    //modal.classList.add('hide');
}

function stopProp(event) { // !!! ВАЖНО !!!
    event.stopPropagation(); //остановить всплытие событий
    // preventDefault() - метод, отключает стандартное поведение
}

function addUser() {
    console.log(addFields);
    const [name, email, age] = addFields; //создаем 3 константы на лету, разбирая addFields
    const emailValue = email.value;
    console.log(emailValue);

    const patternEmail = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/gmi;
    const patternName = /^([a-z]+ [a-z]+)$/gmi;
    const patternAge = /^(?:1(?:00?|\d)|[2-5]\d|[6-9]\d?)$/gm; // | - или в регулярном выражении

    const isEmailValid = patternEmail.test(email.value);
    const isNameValid = patternName.test(name.value);
    const isAgeValid = patternAge.test(age.value);

    //console.log(isAgeValid);

    clearModalErrors();
    if (isEmailValid && isNameValid && isAgeValid) {
        const newUser = createUser(name.value, email.value, age.value); //важен порядок передачи данных в функцию
        users.push(newUser);
        renderUsers(users);
        clearModalFields();
        closeModal();
        scrollToLast();

    } else {
        name.classList.add(isNameValid ? 'valid' : 'error'); //Подсветка некоректно заполненных областей
        email.classList.add(isEmailValid ? 'valid' : 'error');
        age.classList.add(isAgeValid ? 'valid' : 'error');
    }
    /*
    const pattern = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/gmi; //РЕГУЛЯРНЫЕ ВЫРАЖЕНИЯ пишутся внутри /здесь/
    const isAccept = pattern.test(email.value); //метод test применяем на начении email.value

    console.log(isAccept);

    const str = emailValue.replace('@', 'собака'); //замена символа "@" на слово "собака"
    //const str = emailValue.replace(/@/gmi', 'собака'); //замена символа "@" на слово "собака"
    console.log(str);
    */
}

function clearModalErrors() {
    addFields.forEach(field => field.classList.remove('error')); //Очистка в массиве addFileds удаляет при наличии стиль 'error'
}

function createUser(name, email, age) { //Создаем обьект
    const [first, last] = name.split(' '); //Разделить name по пробелу и записать в first и last
    const newUser = {
        email,
        dob: {
            age
        },
        name: {
            first,
            last
        },
        login: {
            uuid: Math.random().toString() //сгенерировать случайный uuid и сделать его строкой
        },
        picture: {
            medium: './images/stub.png'
        }
    };

    return newUser;
}

function clearModalFields() { //очистить поля
    addFields.forEach(item => item.value = '');
}

function scrollToLast() {
    setTimeout(() => {
        const lastIndex = listBlock.children.length - 1; //длина listBlock минус 1
        listBlock.children[lastIndex].scrollIntoView({ behavior: 'smooth', block: 'center' }); //block - куда постарать оборазить в видимой области по центру
    }, 0);
}

function changeGender(event) {
    const gender = event.target.value;
    if (gender === 'all') {
        renderUsers(users);
    } else {
        const filteredUsers = users.filter(item => item.gender === gender);
        //console.log(users[0]);
        renderUsers(filteredUsers);
    }
}


function renderGenderOptions(users) {
    const allGenders = users.map(item => item.gender);
    //console.log(allGenders);
    //const temp = new Set(allGenders); //new - конструктор // set - сделать обьект с уникальными ключами
    const uniqueGenders = Array.from(new Set(allGenders)); //сделать чистый массив
    //console.log(temp);
    uniqueGenders.unshift('all');

    const optionTags = uniqueGenders.map(item => {
        const option = getTag('option', 'option', item);
        option.value = item;

        return option;
    });

    genderDropdown.append(...optionTags);
}

function sortAsc() { //соритровка имен
    const newUsers = [...users];
    newUsers.sort((a, b) => {
        return a.name.first.localeCompare(b.name.first);
    });
    renderUsers(newUsers);
}

function sortDesc() {
    const newUsers = [...users];
    newUsers.sort((a, b) => {
        return b.name.first.localeCompare(a.name.first);
    });
    renderUsers(newUsers);
}

/*Сортировка по имени
function sortAsc() {
    const newUsers = [...users];
    users.sort((a, b) => a.dob.age - b.dob.age); //меняет исходный массив, но в данном случае сбивается сортировка при выборе пола
        /* или так
        if (a.dob.age > b.dob.age) {
            return 1;
        }
        if (a.dob.age < b.dob.age) {
            return -1;
        }
        return 0;
    });
    renderUsers(newUsers);
}

function sortDesc() {
    const newUsers = [...users];
    users.sort((a, b) => b.dob.age - a.dob.age);
    renderUsers(newUsers);
}
*/


/*Домашнее задание #1
JSON Viewer - расширение для Chrome для просмотра api-запросов
1) Сделать тоже самое на вывод по запросу пользователей с сайта
https://rickandmortyapi.com/documentation/

https://randomuser.me/

2) Зарегестрироваться на сервисе погоды. который дает api-запросы. Чтобы была возможность вывести данные по погоде на какой-либо город на неделю.
navigator.geolocation


Видео на ютубе по js - Владилен Минин
*/


/*Домашнее задание #2
Большой кусок текста, сгенерировать онлайн-текст и добавить в случайные места с разным видом отображения этих чисел
150 5050 00$
1 2000 00 byn
1200 300BYN
20000ByN

разбить по разрядам и byn - строчными
*/

/*
для input есть ключ value, которое хранит значение
*/