const xhr = new XMLHttpRequest();
//Инструмент (api браузера), через который делается запрос на бэкэнд. Класс new...


const listBlock = document.querySelector('.list');
//Метод querySelector() возвращает элемент на index.html. Если совпадений не найдено, возвращает значение null.


listBlock.classList.add('loading');
/*
Свойство classList возвращает псевдомассив DOMTokenList, содержащий все классы элемента.

ClassList является геттером. Возвращаемый им объект имеет несколько методов:
    add( String [,String] )          - Добавляет элементу указанные классы
    remove( String [,String] )       - Удаляет у элемента указанные классы
    item ( Number )                  - Результат аналогичен вызову сlassList[Number]
    toggle ( String [, Boolean])     - Если класс у элемента отсутствует - добавляет, иначе - убирает. 
    Когда вторым параметром передано false - удаляет указанный класс, а если true - добавляет.
    Если вторым параметром передан undefined или переменная с typeof == 'undefined', поведение будет аналогичным передаче только первого параметра при вызове toggle.
    contains ( String )              - Проверяет, есть ли данный класс у элемента (вернет true или false)
*/


listBlock.addEventListener('click', onListClick); //вешаем на клике вызов функции
/*
Метод EventTarget.addEventListener() регистрирует определенный обработчик события, вызванного на EventTarget.
EventTarget может быть Element, Document, Window, или любым другим объектом, поддерживающим события (таким как XMLHttpRequest).
*/


let users = [];


xhr.open('GET', 'https://rickandmortyapi.com/api/character?results=10'); //используются методы GET, POST, PUT, DELETE, PATCH
//Статусы запросов 200 - ответ получен, 400 - не найдено, 500 - запрос верный, но беда на сервере
//?results=10&gender=male - запрос на 10 пользователей-мужчин с бэка

xhr.onreadystatechange = function() { //функция показывает когда отрабатывает сервер и срабатывает при каждом новом запросе
//xhr.addEventListener('readystatechange', func); //callback функция на каждое изменение статуса, на которую повесили слушателя, но по факту это одна и та же функция. что и выше
    //console.log('State change...', xhr.readyState); //вывести на консоль статусы изменения/готовности (0-4) - 4 запрос успешен, 3 - идет ответ от сервера, 2 - запрос отправлен
    //console.log('State change...', xhr.status); //вывести на консоль статус

    if (xhr.readyState === 4) { //проверка. что запрос выполнен
        if (xhr.status === 200) { //проверка, что данные получены
            const response = xhr.responseText; //сохраняем данные от сервера в виде строки
            const data = JSON.parse(response); //сохраняем данные от сервера как массив
            users = data.results;
            renderUsers(data.results);
            listBlock.classList.remove('loading');
        }
    }
}

xhr.send(); //запуск запроса на сервер

function getUserCard(user) { //если в функции есть get, то должен быть return
    //const card = document.createElement('div');
    //card.classList.add('user');
    const card = getTag('div', 'user');
    const info = getTag('div', 'info');
    //const fullName = user.name.first + ' ' + user.name.last;
    const name = getTag('span', 'name', user.name);
    const email = getTag('span', 'email', user.species);
    const age = getTag('span', 'age', user.status);
    const avatar = getTag('img', 'avatar', user.image);
    const deleteBtn = getTag('div', 'delete', 'X');
    //avatar.src = user.picture.medium;

    //card.id = user.login.uuid; //в div с классом user добавляем уникальный ключ uuid

    info.append(name, email, age); //добавли в div.info
    card.append(avatar, info, deleteBtn); //добавили

    return card;
}

function getTag(tagName, className, content=null) { //функция создания элементов на странице
    //null - значение по умолчанию, и оно становится необязательным
    const newTag = document.createElement(tagName); //tagName - какой элемент создать в HTML: div, span, img...
    newTag.classList.add(className); //className - какие стили использовать для стилизации
    newTag.innerText = content;

    return newTag;
}

function renderUsers(users) {
    listBlock.innerHTML = '';
    // users.forEach(item => {
    //     const userCard = getUserCard(item);
    //     listBlock.append(userCard);
    // });

    const userCards = users.map(item => getUserCard(item)); // метод map - на основании исходного массива - формирует новый массив, либо возвращает пустой массив если ничего не сделали
    //item => getUserCard(item) - передаем на каждой итерации передает данные в userCard
    listBlock.append(...userCards);//разбираем массив на элементы
}


function onListClick(event) { //проверяем, где произошел клик
    //console.log(event);
    if (event.target.className === 'delete') { //проверка на то, что клик произошел клик на div.delete
        //console.log(event.target.parentNode.id);//ищем уникальный id для удаления
        const id = event.target.parentNode.id; //забираем id для удаления
        deleteUser(id);
    }
}

function deleteUser(userId) {
    users = users.filter(item => item.login.uuid !== userId); //удаление быстрым способом через метод filter 
    renderUsers(users);}

/*Домашнее задание
JSON Viewer - расширение для Chrome для просмотра api-запросов
1) Сделать тоже самое на вывод по запросу пользователей с сайта
https://rickandmortyapi.com/documentation/

https://randomuser.me/

2) Зарегестрироваться на сервисе погоды. который дает api-запросы. Чтобы была возможность вывести данные по погоде на какой-либо город на неделю. 
navigator.geolocation


Видео на ютубе по js - Владилен Минин
*/