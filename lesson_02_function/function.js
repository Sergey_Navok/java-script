let a = 10;
let b = 7;
let summ = 0;


//Работа внутри функции с глобальной переменной считается НЕ ПРАВИЛЬНЫМ!
function getSumm(num1, num2) {
    const summ = num1 + num2;
    console.log(summ);
    return summ;
}

function getSqrt(num1, num2) {
    const result = num1 / num2;
    console.log(result);
}

summ = getSumm(a, b);
console.log(summ);

console.log(getSqrt(a, b));


switch (a) {
    case 10:
        console.log('Hello');
        break;

    default:
        console.log('Bye');
        break;
}


function sayHello(name = 'noName', age = 30) {//name = - с равно функция необязательная, без развно - обязательная
    console.log('Hello ', name, age);
}

//sayHello('Sergey'); //Объявлять функцию можно в любом место, но чаще всего принято - функции пишут внизу


//ДЗ сделать функцию. которое принимать будет число, а бдет возвращать текстовое описание