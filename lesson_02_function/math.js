let a = 2.6;
let b = [5, 2, 1, 9]

const result = Math.min(...b); //спред, деструктурированный массив

console.log(result);


const random = Math.random();
console.log(random);

function getRandom(from, to) {
    const random = from - 0.5 + Math.random()*(to - from + 1);
    return Math.round(random);
}

const r = getRandom(0, 100);
console.log(r);