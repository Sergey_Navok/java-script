let arr = [1, 4, 5, 2, 9, 3, 0, 5, 1];

//for (let i = 0; i< arr.length; i++) {
    //console.log(arr[i]);
//}

/*
function showItem(item, index, self) {
    console.log(item, index, self);

}


//безыменная функция
arr.forEach(function (item, index, self) {
    console.log(item, index, self);

});

//стрелочная функция
arr.forEach((item, index, self) => {
    console.log(item, index, self);

});

//упрощенный вид
arr.forEach((item, index, self) => console.log(item, index, self));

//если только одно значение
arr.forEach(item => console.log(item)); //не возвращает значения

*/

/*22-----------------------------------
function increment2(value) {
    return value + 2;
}

function increment(value) {
    if (value > 5) {
        return value - 10;
    }
    return value + 2;
}

const incArray = arr.map(increment); //не влияет на исходный массив, а создает новый

const incArray2 = arr.map(value => value + 2);


console.log(arr);
console.log(incArray);
console.log(incArray2);
*/



//упрощенное от 22
const incArray = arr.map(value => {
    if (value > 5) {
        return value - 10;
    }
    return value + 2;
});

const incArray2 = arr.map(value => value + 2);


console.log(arr);
console.log(incArray);
console.log(incArray2);


function getRandom(from, to) {
    const random = from - 0.5 + Math.random()*(to - from + 1);
    return Math.round(random);
}

const randomArr = [];

for (let i=0; i < 15; i++) {
    const random = getRandom(-20, 50)
    randomArr.push(random); // добавляет элемент в конец массива
    // randomArr = [...randomArr, random];
}

console.log(randomArr);

function negative(value) {
    if (value < 0) {
        return true;
    }
    return false;
}


const negativeValues = randomArr.filter(item => item < 0); //возвращает новый массив

console.log(negativeValues);


const firstNegative = randomArr.find(item => item < 0 );//не влияет на исходный массив
const firstNegative2 = randomArr.reverse().find(item => item < 0 );//влияет на исходный массив

console.log(firstNegative);
console.log(firstNegative2);


const isHaveNegative = randomArr.some(item => item <0);//ищет хотя бы одно совпадение
console.log(isHaveNegative);


const isHaveNegative2 = randomArr.every(item => item <0);//проверяет все на соответствие
console.log(isHaveNegative2);

