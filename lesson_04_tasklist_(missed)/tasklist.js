let tasks = [
    {
        id: 55,
        title: 'Купить пива',
        description: '5л светлого',
        date: new Date().getTime(),
        isComplete: false
    },
    {
        id: 66,
        title: 'Уборка в квартире',
        description: 'со шваброй',
        date: new Date().getTime(),
        isComplete: false
    },
    {
        id: 77,
        title: 'Принять шахбалай',
        description: 'От старшего брата',
        date: new Date().getTime(),
        isComplete: true
    },
    {
        id: 88,
        title: 'Сделать хорошо',
        description: 'С девушкой',
        date: new Date().getTime(),
        isComplete: false
    },
    {
        id: 99,
        title: 'Сделать плохо',
        description: 'С кем нибудь',
        date: new Date().getTime(),
        isComplete: true
    },
];


//ЗАНЯТИЕ 4, КОГДА Я БОЛЕЛ
const containerTag = document.querySelector('.container');
const addButton = document.querySelector('#add');
addButton.addEventListener('click', addTask);
renderTasks(tasks);

// function getTaskCard(taskObj) {
//     const frendlyDate = moment (taskObj.date).format('DD/MM/YYYY HH:mm');
//     const checked = taskObj.isComplete ? 'checked' : '';
//     const taskCardHtml = 
//     `<div>
//         <div class="task">
//             <input type="checkbox" ${checked}>

//             <div class="info">
//                 <span class="title">${taskObj.title}</span>
//                 <span class="subs">${taskObj.description}</span>
//             </div>
//             <span class="datetime">${frendlyDate}</span>
//         </div>
//     </div>`
//     return taskCardHtml;
// }

function createTag(tagName, className, value) {
    const tag = document.createElement(tagName);
    tag.classList.add(className);
    if (value) {
        tag.innerText = value;
    }

    return tag;
}



function getTaskCard(taskObj) {

    const taskHtml = createTag('div', 'task');

    const inputHtml = document.createElement('input');
    inputHtml.type = 'checkbox';
    inputHtml.checked = taskObj.isComplete;

    const infoHtml = createTag('div', 'info');

    const titleHtml = createTag('span', 'title', taskObj.title);

    const subsHtml = createTag('span', 'subs', taskObj.description);

    const frendlyDate = moment(taskObj.date).format('DD/MM/YYYY HH:mm');
    const timeHtml = createTag('span', 'datetime', frendlyDate);

    const deleteBtn = createTag('div', 'delete', 'x');
    deleteBtn.id = taskObj.id;
    deleteBtn.addEventListener('click', deleteItem);

    infoHtml.appendChild(titleHtml);
    infoHtml.appendChild(subsHtml);
    taskHtml.appendChild(inputHtml);
    taskHtml.appendChild(infoHtml);
    taskHtml.appendChild(timeHtml);
    taskHtml.appendChild(deleteBtn);


    return taskHtml;
}


function renderTasks(tasksArr) {
    containerTag.innerHTML = '';
    tasksArr.forEach(item => {
        const taskCardHtml = getTaskCard(item);
        containerTag.appendChild(taskCardHtml);
    });
}

function deleteItem(event) {
    //tasks.splice(index, 1);
    const id = +event.target.id;
    // const currentTask = tasks.find(item => item.id === id);
    // const index = tasks.indexOf(currentTask);
    // tasks.splice(index, 1);
    tasks = tasks.filter(item => item.id !== id);

    renderTasks(tasks);
}


function addTask() {
    const titleInput = document.querySelector('#title');
    const descriptionInput = document.querySelector('#description');

    const title = titleInput.value;
    const description = descriptionInput.value;

    const newTask = {
        id: 44,
        title,
        description,
        date: new Date().getTime(),
        isComplete: false
    };

    console.log(newTask);
}