const date = new Date();

console.log(date.getTime());//unix время
console.log(date.getMonth());//месяца в JS идут с 0 до 11


function getTime () {
    const date = new Date();
    const hours = addZero(date.getHours());
    const min = addZero(date.getMinutes());
    const sec = addZero(date.getSeconds());

    //const timeString = hours + ':' + min + ':' + sec;
    const timeString = `${hours}:${min}:${sec}`;


    console.log(timeString);
    document.write(timeString);
}

function addZero(value) {
    if (value < 10) {
        return `0${value}`;
    } else {
        return value;
    }
    // return value < 10 ? `0${value}` : value;
}

getTime();//Запуск функции

moment.locale('ru');
const curretTime = moment().format('HH---mm---ss///dddd');
console.log(curretTime);


setInterval(getTime, 1000);

setInterval(() => (console.log('Hello')), 1000);//У МЕНЯ НЕ БЫЛО ЭТОЙ СТРОКИ

/*
Api - возможности браузера

setTimeout()
                        ms
setInterval(function, time )
                        1000

*/
