let arr= [1, 2, 3, 4, 5, 6, 7, 8];

/*for(let i=0; i<arr.length; i++){
    console.log(arr[i]);
}*/


arr.forEach(item => console.log(item)); // не возвращает значение

//const incrArr = arr.map(value => value + 2); // не влияет на исходный массив, а создает новый
const incrArr = arr.map(value => {
    if (value>5){
        return value - 10;
    }
    return value + 2;
});



console.log(arr);
console.log(incrArr);


function getRandom(from, to){
    const random= from -0.5 +Math.random()*(to-from+1);
    return Math.round(random);
}

const randomArr = [];

for(let i=0; i<15; i++){
    const random = getRandom(-20, 50);
    randomArr.push(random); // добавляет элемент в конец массива
    // randomArr = [...randomArr, random]; 
}

console.log(randomArr);

/*
function negative(value){
    if(value<0){
        return true;
    }
    return false;
}
*/

const negativeValues = randomArr.filter(item => item < 0);  //возвращает новый массив

console.log(negativeValues);

const firstNegative = randomArr.find(item => item <0);

console.log(firstNegative);

const isHaveNegative = randomArr.every(item => item < 0);   // every - строго, some - ищет хотя бы одно по критерию

console.log(isHaveNegative);


const summ = arr.reduce((accum, item) => accum += item, 0);

console.log(summ);

/* 
метод pop() - удаляет последний эл-т из массива и возвращает его
      push(item) - добавляет эл-т в конец массива
      shift() - удаляет первый эл-т массива, и возвращает его
      unshift()- добавляет эл-т в начало массива
      splice(index, count, item)
      .indexOf(item) - возвращает индекс искомого item в массиве
                     - если не нашел, то возвращает -1 
*/

//console.log(arr);
//arr.splice(2, 4);
//console.log(arr);

/*let number;
let temp;
number = prompt("Введите искомое значение: ");
temp = prompt("Введите число которое замент искомое.");
const currentnumber = Number(number);
const currenttemp = Number(temp);

console.log(arr);
function replaceNum(currentnumber, currenttemp) {
    const index = arr.indexOf(currentnumber);
    if(index >= 0){
        arr.splice(index, 1, currenttemp)
    }
    return arr;
}

replaceNum(currentnumber, currenttemp);
console.log(arr);
*/

function replaceAll(search, replace, inc_arr){
    const arr = [...inc_arr];
    return arr.map(item => {
        if(item === search){
            return replace;
        }
        return item;
    });
}

arr = replaceAll(1, 200, arr);
console.log(arr);

