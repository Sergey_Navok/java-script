import { TaskComponent } from './components/task/task.component';
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [
    TaskComponent,
    AboutComponent
  ]
  imports: [
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: 'tasks',
        pathMatch: 'full'
      },
      {
        path: 'task',
        component: TaskComponent
      },
      {
        path: 'about',
        component: AboutComponent
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})

export class RoutingModule {

}
