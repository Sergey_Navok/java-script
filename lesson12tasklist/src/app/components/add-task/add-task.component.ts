import { Task } from './../../interfaces/task.interface';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {

  public taskForm: FormGroup;
  public taskName: FormControl;

  @Output() public newTaskAdd = new EventEmitter<Task>();

  constructor() { }

  ngOnInit(): void {
    this.initFields();
    this.initForm();
  }

  public addTask(): void {
    if (this.taskForm) { //проверяем вю форму на валидность
     // console.log(this.taskForm.value);
      const title = this.taskName.value;
      const task: Task = {
        id: Math.random(),
        title, //title: this.taskForm.value.taskName - равнозначное в данном случае
        complete: false
      };

      this.newTaskAdd.next();

      this.clearForm();
    }
  }

  public keyPressHandler(event: KeyboardEvent): void { ///ИЗМЕНИТЬ
    //console.log(event.);
    if (event.code === 'Enter') {
      this.addTask();
    }
  }

  private clearForm(): void {
    this.taskName.setValue('');
    this.taskName.markAsUntouched();//пометить, как нетронутое
  }

  private initFields(): void {
    this.taskName = new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]); //валидатор - пока мы не заполним форму. ее нельзя будет отправить
  }

  private initForm(): void {
    this.taskForm = new FormGroup({
       taskName: this.taskName
    });
  }

}
