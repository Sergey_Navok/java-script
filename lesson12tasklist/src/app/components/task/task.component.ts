import { Task } from './../../interfaces/task.interface';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component ({
  selector: 'app-task', //свойство селектор необязательное и в нем указывается имя будущего компонента
  templateUrl: 'task.component.html',
  styleUrls: ['task.component.scss']
})

export class TaskComponent {
  @Input() public task: Task; //task - имя переменной, Task импортируем из task.interface.ts
  @Output() deleteEvent = new EventEmitter<number>();
  //public isEdit: boolean;
  static editTaskId: number;

  public deleteTask(): void {
    this.deleteEvent.next(this.task.id);
  }

  public toggleEdit(): void {
    //this.isEdit = !this.isEdit;
    if (this.isEdit) {
      TaskComponent.editTaskId = null;
    } else {
      TaskComponent.editTaskId = this.task.id;
    }

  }

  public get isEdit(): boolean {
    //console.log(123);
    return TaskComponent.editTaskId === this.task.id;
  }
}
