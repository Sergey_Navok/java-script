import { EnterPressDirective } from './../../directives/enter.directive';
import { FirstDirective } from './../../directives/first.directive';
import { AddTaskComponent } from './../../components/add-task/add-task.component';
import { DateTimePipe } from './../../pipes/datetime.pipe';
import { TaskComponent } from './../../components/task/task.component';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

@NgModule({
declarations: [
  TaskComponent,
  DateTimePipe,
  AddTaskComponent,
  FirstDirective,
  EnterPressDirective

],
imports: [
  CommonModule
]
})

export class TasksModule {

}
