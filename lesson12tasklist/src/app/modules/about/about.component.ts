import { DataService } from './../../services/data.service';
import { Task } from './../../interfaces/task.interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})

export class AppComponent implements OnInit {

  public taskList: Task[] = [];
  //public taskList: Observable<Task[]>;

  //to way data binding двухстороння привязка данных
  public testValue = 'Text';

  public addOne(): void {
    this.testValue += '1';
  }

  constructor(private dataService: DataService) {
  } //перечисялем сервисы, которые мы используем, переменные называются также как и сервис, но с маленькой буквы. Тело конструктора лучше держать пустым
  //сколько модулей мы подключаем в сервис - столько экземпляров сервисов и создается

  public ngOnInit(): void {
    //console.log('App component inited');

    this.dataService.getTask().subscribe(data => this.taskList = data);
    //this.taskList = this.dataService.getTask();
  }

  public deleteItem(id: number): void {
    //this.taskList = this.dataService.deleteTask(id);
    this.taskList = this.taskList.filter(item => item.id !== id);
  }

  public addNewTask(task: Task): void {
    //this.taskList.push(task);
    this.taskList = [...this.taskList, task]; //Более правильный вариант - деструктурируем сущ массив и добавляем в конец новую задачу
  }
}
