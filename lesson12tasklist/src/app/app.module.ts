import { RoutingModule } from './app.routing.module';
import { EnterPressDirective } from './directives/enter.directive';
import { FirstDirective } from './directives/first.directive';
import { DateTimePipe } from './pipes/datetime.pipe';
import { TaskComponent } from './components/task/task.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AddTaskComponent } from './components/add-task/add-task.component';
import { AboutComponent } from './modules/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    DateTimePipe,
    AddTaskComponent,
    FirstDirective,
    EnterPressDirective
  ],
  imports: [
    BrowserModule,
    FormsModule, //используется для двухсторонней привязки
    HttpClientModule,
    ReactiveFormsModule, //реактивные формы
    RoutingModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
