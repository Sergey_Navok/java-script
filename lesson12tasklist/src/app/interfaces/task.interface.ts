export interface Task {
  id: number;
  title: string;
  complete: boolean;
}

/*
//GENERIC
export interface Task<T> {
  id: number;
  name: string;
  description: string;
  datetime: T;
  isComplete: boolean;
}

const item: Task<string> = {
  id: 0,
  name: 'Test',
  description: 'wergsrg',
  datetime: '2122',
  isComplete: false
}
*/



/*
export interface AddTask extends Task {
  addTime: number;
}
*/

/*
export interface AddTask {
  id: number;
  name: string;
  description: string;
  datetime: number;
  isComplete: boolean;
  addTime: number;
}
*/
