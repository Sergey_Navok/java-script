/*
const user = {
    id: 11231,
    name: {
        first: 'Ivan',
        last: 'Ivanov',
    },
    email: 'test@mail.ru',
    age: 25
};

user.name.first = 'Fedor';

const fullName = user.name.first + ' ' + user.name.last;

console.log(user);
console.log(fullName);
*/

const users = [
    {
        id: 11231,
        name: {
            first: 'Ivan',
            last: 'Ivanov',
        },
        email: 'test@mail.ru',
        age: 25
    },
    {
        id: 11232,
        name: {
            first: 'Fedor',
            last: 'Fedorov',
        },
        email: 'test@yandex.ru',
        age: 30
    },
    {
        id: 11233,
        name: {
            first: 'Petr',
            last: 'Petrov',
        },
        email: 'test@gmail.ru',
        age: 18
    },
    {
        id: 11234,
        name: {
            first: 'Sidr',
            last: 'Sidorov',
        },
        email: 'test@tut.by',
        age: 50
    },
    {
        id: 11234,
        name: {
            first: 'Nikolay',
            last: 'Nikolaev',
        },
        email: 'test@tut.by',
        age: 35
    }
];

//Поиск по имени
function equalName(item, index, self) {
    /*
    if (item.name.first === 'Petr') {
        return true;
    } else {
        return false;
    }
    */

    //Тоже самое
    return item.name.first === 'Petr';    
}

//Стрелочная функция
function getUserByName(name) {
    const user = users.find(item => item.name.first === name);
    return user;
}

//Вывод на консоль
const user = getUserByName('Petr');
console.log(user);

/*
ДЗ на основании исходного объекта юзеров сделать новый массив имен и фамилей
Найти пользователя с возрастом от 20 до 25 лет
найти пользователя с возрастом больше 30
Создать фнкцию удаления пользователя из массива по его id из обьекта/массива

ПОЧИТАТЬ ПРИНЦИП РАЗРАБОТКИ SOLID
*/
