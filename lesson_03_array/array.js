let arr = [1, 4, 5, 2, 9, 3, 0, 5, 1];

//LESSON 03

/*
function accumSumm(accum, item) {
    return accum += item;
}

const summ=arr.reduce (accumSumm, 0);

console.log(summ)
*/

//Упрощение функции
const summ=arr.reduce ((accum, item) => accum += item, 0);

console.log(summ)

/*
.pop() - удаляет последний элемент из массива и возвращает его
const lastItem = arr.pop() - можно записать в const lastItem последний элемент перед уго удалением из массива
    arr.push(item) - добавляет элемент в конец массива

.shift() - удаляет первый элемент массива и возвращает его
const lastItem = arr..shift()
    .unshift(item) - добавляет элемент в начало массива

.splice(index, count) .splice(3, 1)

.indexOf(item) - возвращает индекс искомого item в массиве, если не нашел, то возвращает -1
*/

const lastItem = arr.pop();//можно посмотреть методы shift или push

console.log(arr);
console.log(lastItem);

arr.push(100);
console.log(arr);

console.log(arr);
arr.splice(2, 1);//2 - с какого элемента начать, 1 - сколько элментов удалить
console.log(arr);

console.log(arr);
arr.splice(2, 1, 100);//2 - с какого элемента начать, 1 - сколько элментов удалить, 100 - на что заменить первый удаленный элемент (когда 1 больше 1)
console.log(arr);

console.log(arr);
arr.splice(2, 1, ...[100, 100]);//2 - с какого элемента начать, 1 - сколько элментов удалить, 100 - на что заменить первый удаленный элемент (когда 1 больше 1)
console.log(arr);



function replaceNum(find, rename, myarr) {
    const incomingArr = [...myarr];
    const index = incomingArr.indexOf(find);
    incomingArr.splice(index, 1, rename);
    return incomingArr;
}

arr = replaceNum(3, -100, arr);
console.log(arr);




function replaceAll(search, replace, inc_arr) {
    const arr = [...inc_arr];
    return arr.map(item => {
        if (item === search) {
            return replace;
        }
        return item;
    });
}

arr = replaceAll (100, 200, arr);
console.log(arr);