let city = prompt('Где будем смотреть погоду?');

const date = new Date();
moment.locale('ru');
//const currentDay = moment().format('dddd'); //День недели
//console.log(currentDay);
//const currentDate = moment().format("DD MMM YYYY"); //Число-мес-год
//console.log(currentDate);


//const weatherBlock = document.querySelector('.card');
const currentBlock = document.querySelector('.current');
const locateBlock = document.querySelector('.position');
const detalsBlock = document.querySelector('.detals');
//weatherBlock.classList.add('loading');

const xhr = new XMLHttpRequest(); //Инструмент (api браузера), через который делается запрос на бэкэнд. Класс new
const list = document.querySelector('.weathercard');

//let city = 'gomel';
let adress = 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&units=metric&lang=ru&appid=a59d92b2463e709a42c5ef931072ca70';
//let currentWeather = [];

xhr.open('GET', adress); //используются методы GET, POST, PUT, DELETE, PATCH
//xhr.open('GET', 'https://randomuser.me/api?results=100');
//Статусы запросов 200 - ответ получен, 400 - не найдено, 500 - запрос верный, но беда на сервере

xhr.onreadystatechange = function () { //функция показывает, когда отрабатывает сервер и срабатывает при каждом новом запросе на него
    //xhr.addEventListener('readystatechange', func); //callback функция на каждое изменение статуса, на которую повесили слушателя, но по факту это одна и та же функция. что и выше
    //console.log('State change...', xhr.readyState); //вывести на консоль статусы изменения/готовности (0-4) - 4 запрос успешен, 3 - идет ответ от сервера, 2 - запрос отправлен
    //console.log('State change...', xhr.status); //вывести на консоль статус

    if (xhr.readyState === 4) { //проверка. что запрос выполнен
        if (xhr.status === 200) { //проверка, что данные получены
            const jsonResponse = xhr.responseText; //сохраняем данные от сервера в виде строки
            const currentWeather = JSON.parse(jsonResponse); //сохраняем данные от сервера как массив

            console.log(currentWeather); //Обьект с данными от сервера
            const currentPosition = currentWeather.name + ', ' + currentWeather.sys.country;
            console.log(currentPosition); //Город + Страна
            console.log(currentWeather.main.temp); //Текущая температура
            console.log(currentWeather.main.feels_like); //Ощущается как
            console.log(currentWeather.main.temp_max); //Максимальная температура
            console.log(currentWeather.main.temp_min); //Минимальная температура
            console.log(currentWeather.weather[0].description); //Описание
            console.log(currentWeather.weather[0].icon); //Иконка

            
            //weatherBlock.classList.remove('loading');
            renderWeather(currentWeather);
        }
    }
}

xhr.send(); //запуск запроса на сервер

function renderWeather(currentWeather) {
    //День недели
    const day = document.createElement('div');
    day.classList.add('day');
    day.innerText = moment().format('dddd'); //День недели
    currentBlock.append(day);

    //Дата
    const date = document.createElement('div');
    date.classList.add('date');
    date.innerText = moment().format("DD MMM YYYY"); //Число-мес-год
    currentBlock.append(date);

    //Регион
    const currentPosition = document.createElement('div');
    currentPosition.classList.add('locate');
    currentPosition.innerText = currentWeather.name + ', ' + currentWeather.sys.country;
    locateBlock.append(currentPosition);

    //Иконка
    const icon = document.createElement('img');
    icon.classList.add('icon');
    icon.src = 'http://openweathermap.org/img/wn/' + currentWeather.weather[0].icon + '@2x.png';
    currentBlock.append(icon);

    //Текущая температура
    const temp = document.createElement('div');
    temp.classList.add('temperature');
    temp.innerText = currentWeather.main.temp + '°C';
    currentBlock.append(temp);

    //Описание
    const description = document.createElement('div');
    description.classList.add('description');
    description.innerText = currentWeather.weather[0].description;
    currentBlock.append(description);

    //Ощущается как
    const feelsLike = document.createElement('div');
    feelsLike.classList.add('text');
    feelsLike.innerText = 'Ощущается как: ' + currentWeather.main.feels_like + '°C';
    detalsBlock.append(feelsLike);

    //Влажность
    const humidity = document.createElement('div');
    humidity.classList.add('text');
    humidity.innerText = 'Влажность: ' + currentWeather.main.humidity + '%';
    detalsBlock.append(humidity);

    //Облачность
    const clouds = document.createElement('div');
    clouds.classList.add('text');
    clouds.innerText = 'Облачность: ' + currentWeather.clouds.all + '%';
    detalsBlock.append(clouds);

    //Атмосферное давление
    const pressure = document.createElement('div');
    pressure.classList.add('text');
    pressure.innerText = 'Атмосферное давление: ' + currentWeather.main.pressure + 'гПа';
    detalsBlock.append(pressure);

    //Скорость ветра
    const wind = document.createElement('div');
    wind.classList.add('text');
    wind.innerText = 'Скорость ветра: ' + currentWeather.wind.speed + 'м/с';
    detalsBlock.append(wind);

    //Максимальная температура
    const tempMax = document.createElement('div');
    tempMax.classList.add('text');
    tempMax.innerText = 'Макс температура: ' + currentWeather.main.temp_max + '°C';
    detalsBlock.append(tempMax);

    //Максимальная температура
    const tempMin = document.createElement('div');
    tempMin.classList.add('text');
    tempMin.innerText = 'Мин температура: ' + currentWeather.main.temp_min + '°C';
    detalsBlock.append(tempMin);

    /*
    //Восход
    const sunrise = document.createElement('div');
    sunrise.classList.add('text');
    sunrise.innerText = 'Восход: ' + currentWeather.sys.sunrise;
    detalsBlock.append(sunrise);

    //Закат
    const sunset = document.createElement('div');
    sunset.classList.add('text');
    sunset.innerText = 'Закат: ' + currentWeather.sys.sunset;
    detalsBlock.append(sunset);

    //Закат
    const sunset = document.createElement('div');
    sunset.classList.add('text');
    cosnt sunsetTime = currentWeather.sys.sunset;
    moment().format('LT');
    sunset.innerText = 'Закат: ' + currentWeather.sys.sunset;
    detalsBlock.append(sunset);
    */
}